--1. Liệt kê danh sách các orders ứng với tổng tiền của từng hóa đơn. Thông
--tin bao gồm OrderID, OrderDate, Total. Trong đó Total là Sum của
--Quantity * Unitprice, kết nhóm theo OrderID.
	select o.OrderID, OrderDate,Sum(oo.Quantity * oo.UnitPrice) as Total
	from [dbo].[Orders] o join [dbo].[Order Details] oo on o.OrderID = oo.OrderID
	group by o.OrderID , OrderDate 
--2. Liệt kê danh sách các orders mà địa chỉ nhận hàng ở thành phố ‘Madrid’
--(Shipcity). Thông tin bao gồm OrderID, OrderDate, Total. Trong đó
--Total là tổng trị giá hóa đơn, kết nhóm theo OrderID.
	select o.OrderID, OrderDate,Sum(oo.Quantity * oo.UnitPrice) as Total
	from [dbo].[Orders] o join [dbo].[Order Details] oo on o.OrderID = oo.OrderID
	where o.ShipCity = 'Madrid'
	group by o.OrderID,  OrderDate
--3. Sử dụng 2 table [Orders] và [Order Details], hãy viết các truy vấn thống
--kê tổng trị giá các hóa đơn được xuất bán theo :
--- Tháng …
--- Năm …
--- CustomerID …
--- EmployeeID …
--- ProductID …
	select o.OrderID, EmployeeID,Month(o.OrderDate)as Thang, YEAR (o.OrderDate) as Nam,ProductID, CustomerID ,Sum(oo.Quantity * oo.UnitPrice) as Total
	from [dbo].[Orders] o join [dbo].[Order Details] oo on o.OrderID = oo.OrderID
	group by o.CustomerID,o.OrderID, Month(o.OrderDate), YEAR (o.OrderDate) , EmployeeID , ProductID
--4. Cho biết mỗi Employee đã lập bao nhiêu hóa đơn. Thông tin gồm
--EmployeeID, EmployeeName, CountOfOrder. Trong đó CountOfOrder 
--là tổng số hóa đơn của từng employee. EmployeeName được ghép từ
--LastName và FirstName.
	select o.EmployeeID, (oo.FirstName +' ' + oo.LastName) as EmployeeName,COUNT(o.EmployeeID)as CountOfOrder 
	from [dbo].[Orders] o join [dbo].[Employees] oo on o.EmployeeID = oo.EmployeeID
	group by o.EmployeeID, oo.FirstName, oo.LastName
--5. Cho biết mỗi Employee đã lập được bao nhiêu hóa đơn, ứng với tổng
--tiền các hóa đơn tương ứng. Thông tin gồm EmployeeID,
--EmployeeName, CountOfOrder , Total.
	select o.EmployeeID, (e.FirstName +' ' + e.LastName) as EmployeeName,
	COUNT(o.EmployeeID)as CountOfOrder , Sum(oo.Quantity * oo.UnitPrice) as Total
	from ([dbo].[Orders] o join [dbo].[Employees] e on o.EmployeeID = e.EmployeeID)
	join [dbo].[Order Details] oo on o.OrderID = oo.OrderID
	group by o.EmployeeID, e.FirstName, e.LastName
--6. Liệt kê bảng lương của mỗi Employee theo từng tháng trong năm 1996
--gồm EmployeeID, EmployName, Month_Salary, Salary =
--sum(quantity*unitprice)*10%. Được sắp xếp theo Month_Salary, cùmg
--Month_Salary thì sắp xếp theo Salary giảm dần.
	select o.EmployeeID, (e.FirstName +' ' + e.LastName) as EmployeeName,
	MONTH(o.OrderDate) as Month_Salary, sum(oo.Quantity*oo.UnitPrice)*0.1 as Salary
	from ([dbo].[Orders] o join [dbo].[Employees] e on o.[EmployeeID] = e.[EmployeeID])
	 join [dbo].[Order Details] oo on o.OrderID = oo.OrderID
	 group by o.EmployeeID,e.FirstName, e.LastName,MONTH(o.OrderDate)
	 order by Month_Salary desc , Salary desc
--7. Tính tổng số hóa đơn và tổng tiền các hóa đơn của mỗi nhân viên đã bán
--trong tháng 3/1997, có tổng tiền >4000. Thông tin gồm EmployeeID,
--LastName, FirstName, CountofOrder, Total.
	select o.EmployeeID, e.FirstName as FirstName,  e.LastName as LastName ,
	 COUNT(o.OrderID) as CountofOrder, sum(oo.Quantity*oo.UnitPrice) as Total
	from ([dbo].[Orders] o join [dbo].[Employees] e on o.[EmployeeID] = e.[EmployeeID])
	 join [dbo].[Order Details] oo on o.OrderID = oo.OrderID
	 Where MONTH(o.OrderDate) = 3 and YEAR(o.OrderDate) = 1997 and oo.Quantity*oo.UnitPrice > 4000
	 group by o.EmployeeID,e.FirstName,e.LastName
--8. Liệt kê danh sách các customer ứng với tổng số hoá đơn, tổng tiền các
--hoá đơn, mà các hóa đơn được lập từ 31/12/1996 đến 1/1/1998 và tổng
--tiền các hóa đơn >20000. Thông tin được sắp xếp theo CustomerID,
--cùng mã thì sắp xếp theo tổng tiền giảm dần.

--9. Liệt kê danh sách các customer ứng với tổng tiền của các hóa đơn ở từng
--tháng. Thông tin bao gồm CustomerID, CompanyName, Month_Year,
--Total. Trong đó Month_year là tháng và năm lập hóa đơn, Total là tổng
--của Unitprice* Quantity.
--10.Liệt kê danh sách các nhóm hàng (category) có tổng số lượng tồn
--(UnitsInStock) lớn hơn 300, đơn giá trung bình nhỏ hơn 25. Thông tin
--bao gồm CategoryID, CategoryName, Total_UnitsInStock,
--Average_Unitprice.
--11.Liệt kê danh sách các nhóm hàng (category) có tổng số mặt hàng
--(product) nhỏ hớn 10. Thông tin kết quả bao gồm CategoryID,
--CategoryName, CountOfProducts. Được sắp xếp theo CategoryName,
--cùng CategoryName thì sắp theo CountOfProducts giảm dần.
--12.Liệt kê danh sách các Product bán trong quý 1 năm 1998 có tổng số
--lượng bán ra >200, thông tin gồm [ProductID], [ProductName],
--SumofQuatity
--13.Cho biết Employee nào bán được nhiều tiền nhất trong tháng 7 năm 1997
--14.Liệt kê danh sách 3 Customer có nhiều đơn hàng nhất của năm 1996.
--15.Liệt kê danh sách các Products có tổng số lượng lập hóa đơn lớn nhất.
--Thông tin gồm ProductID, ProductName, CountOfOrders.
